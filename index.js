var img = document.getElementsByClassName("img")[0];

//Determine Scroll Direction
var dir = []

function logScrollDirection() {
    var previous = window.scrollY;
    window.addEventListener('scroll', function() {
        window.scrollY > previous ? dir = "down" : dir = "up";
        previous = window.scrollY;
    });
    return dir;
}
logScrollDirection();
//Scroll Direction Determined
//Sticky Nav Start
window.onscroll = function() {
    logScrollDirection();
    expAndStick();
};
var nav = document.getElementById("Nav");
var sticky = nav.offsetTop;

function expAndStick() {
    if ((window.pageYOffset >= sticky) && (dir == "down")) {
        nav.classList.remove("scrollup");
        nav.classList.add("scrolldown");
    } else if ((window.pageYOffset <= sticky) && (dir == "up")) {
        nav.classList.remove("scrolldown");
        nav.classList.add("scrollup");
    }
}
//Sticky Nav End
//Gallery Begin -
// 1.) Click Image
// 2.) Image is displayed on right,
var artR = document.getElementById('artr');

function image(x) {
    var img = ["art/hh1.jpg", "art/milosz.jpg", "art/bruce.png", "art/dog.png", "art/hh2.png", "art/willow.png"]

}

function mimage(x) {
    var img = ["art/driveinmockup.png", "art/wp.png"]

}



var bodClass = document.getElementsByTagName("BODY")[0];

function mNav() {
    return bodClass.classList.toggle("js-menu_activated");
}

// Art Gallery Script
// Open the Modal
function openModal() {
  document.getElementById('artModal').style.display = "block";
}

// Close the Modal
function closeModal() {
  document.getElementById('artModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function image(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("picThumb");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}

// Mockup Gallery Script
// Open the Modal
function mopenModal() {
  document.getElementById('mockModal').style.display = "block";
}

// Close the Modal
function mcloseModal() {
  document.getElementById('mockModal').style.display = "none";
}

var mslideIndex = 1;
mshowSlides(mslideIndex);

// Next/previous controls
function mplusSlides(n) {
  mshowSlides(mslideIndex += n);
}

// Thumbnail image controls
function mImage(n) {
  mshowSlides(mslideIndex = n);
}

function mshowSlides(n) {
  var i;
  var mslides = document.getElementsByClassName("mmySlides");
  var mdots = document.getElementsByClassName("mpicThumb");
  var mcaptionText = document.getElementById("mcaption");
  if (n > mslides.length) {mslideIndex = 1}
  if (n < 1) {mslideIndex = mslides.length}
  for (i = 0; i < mslides.length; i++) {
    mslides[i].style.display = "none";
  }
  for (i = 0; i < mdots.length; i++) {
    mdots[i].className = mdots[i].className.replace(" active", "");
  }
  mslides[mslideIndex-1].style.display = "block";
  mdots[mslideIndex-1].className += " active";
  mcaptionText.innerHTML = mdots[mslideIndex-1].alt;
}